import { Component,OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  show:boolean;
  months=['Jan','Feb','Mar','Apr','May','Dec'];
  yearvalue:any;
  monthvalue:any;
  yearFetch:any;
  years= new Date()
  yearsdetails= this.years.getFullYear();
  yeardata1 = [this.yearsdetails,this.yearsdetails-1,this.yearsdetails-2];
  constructor(private api :HttpClient){
  }
  yearGet(e){
    this.yearvalue=e;
    this.yearShow();
   }
   monthGet(k){
     this.monthvalue=k;
 
   }
   yearShow() {
     this.api.get(`..//assets/collection.json`).subscribe((results: any) => {

          let resultGot = results.filter((result) =>{if(result['year']===this.yearvalue && result['month']===this.monthvalue){return result}})
     
          this.yearFetch=resultGot;
          this.show=true;
          console.log(resultGot)

     
          // let resultGot = results.filter((result) =>{if(result['year']===this.yearvalue && result['month']===this.monthvalue){return result}})
     
          // this.yearFetch=resultGot;
          // this.show=true;
          // console.log(resultGot)
        });
      }
  // ngOnInit() {
  // // this.api.get(`..//assets/collection.json`).subscribe((data: []) => {
  // //     this.items = data;
  // //     this.items=data
  // //     console.log(data); 
  // //      this.items.map(value=>{
  // //     console.log(`month ${value.month} year ${value.year}`)
  // //      })
  // //   });
  // }
}
