import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-object',
  templateUrl: './object.component.html',
  styleUrls: ['./object.component.css']
})
export class ObjectComponent implements OnInit {
   obj =  { 
    "car" : 
      { 
         "color" : "red",
         "model" : "2013"
      },
     "motorcycle": 
      { 
         "color" : "red",
         "model" : "2016"
      },
     "bicycle": 
      { 
         "color" : "red",
         "model" : "2011"
      }
  }
 
  mark=60;
  

  objectKeys = Object.keys;
  myobject= {
    '1' : [ {"name" : "siva" , "age" : "22" }],
    '2' : [ {"name" : "ram" , "age" : "21" }],
    '3' : [ {"name" : "shri" , "age" : "20" }],
    
  }
  constructor() { }

  ngOnInit() {
  }

}
